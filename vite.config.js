import {defineConfig, loadEnv} from 'vite'

export default defineConfig(({mode}) => {
  const env = loadEnv(mode, process.cwd(), '')
  if (mode === 'production') {
    return {
      base: env.BASE_URL
    }
  } else {
    return {}
  }
})
