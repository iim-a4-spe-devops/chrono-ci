const point = require('../src/point');

test('return point', () => {
  const result = point.point(2);
  expect(result).toMatch(/^2 point/);
});

export {}